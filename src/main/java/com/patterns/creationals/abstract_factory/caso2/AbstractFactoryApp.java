package com.patterns.creationals.abstract_factory.caso2;

public class AbstractFactoryApp {

    int tipo = 3;

    public static void main(String[] args) {
        AbstractFactoryApp app = new AbstractFactoryApp();
        app.liquidar();
    }

    private void liquidar(){
        Factory factory = new PrincipalFactory();
        LiquidacionFactory liquidacionFactory = factory.obtenerFactory(tipo);
        Liquidacion liquidacion = liquidacionFactory.crearLiquidacion();
        liquidacion.liquidar();
    }


}
