package com.patterns.creationals.builder.caso3;

import java.util.Date;

public class BuilderApp {

    public static void main(String[] args) {
        Task task = new TaskBuilder(5l).setDescription("Hello").setSummary("Test").setDueDate(new Date()).build();
        System.out.println(task);
    }
}
