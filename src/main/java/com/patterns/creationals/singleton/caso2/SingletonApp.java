package com.patterns.creationals.singleton.caso2;

public class SingletonApp {
    public static void main(String[] args) {
        StaticBlockSingleton singleton = StaticBlockSingleton.getInstance();
        System.out.println(singleton);
    }
}
