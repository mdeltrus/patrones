package com.patterns.creationals.singleton.caso4;

public class SingletonApp {

    public static void main(String[] args) {
        ThreadSafeSingleton singleton = ThreadSafeSingleton.getInstance();
        System.out.println(singleton);
    }
}
