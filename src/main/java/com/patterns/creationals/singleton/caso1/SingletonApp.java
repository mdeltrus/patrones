package com.patterns.creationals.singleton.caso1;

public class SingletonApp {

    public static void main(String[] args) {
        EagerInitializedSingleton singleton = EagerInitializedSingleton.getInstance();
        System.out.println(singleton);
    }

}
