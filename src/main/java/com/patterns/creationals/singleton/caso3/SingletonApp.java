package com.patterns.creationals.singleton.caso3;

public class SingletonApp {
    public static void main(String[] args) {
        LazyInitializedSingleton singleton = LazyInitializedSingleton.getInstance();
        System.out.println(singleton);
    }
}
