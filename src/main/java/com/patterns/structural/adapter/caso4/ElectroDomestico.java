package com.patterns.structural.adapter.caso4;

public interface ElectroDomestico {

    boolean isEncendida();
    void encender();
    void apagar();
}
