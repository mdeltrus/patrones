package com.patterns.structural.adapter.caso4;

public class AdapterApp {

    public static void main(String[] args) {

        ElectroDomestico lampara = new Lampara();
        lampara.encender();
        System.out.println(lampara);

        ElectroDomestico computadora = new Computadora();
        computadora.encender();
        System.out.println(computadora);

        ElectroDomestico lamparaInglesa = new LamparaAdapter(new LamparaInglesa());
        lamparaInglesa.encender();
        System.out.println(lamparaInglesa);

    }
}
