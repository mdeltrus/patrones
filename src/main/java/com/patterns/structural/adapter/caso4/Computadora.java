package com.patterns.structural.adapter.caso4;

public class Computadora implements ElectroDomestico {

    private boolean encendida;

    public boolean isEncendida() {
        return encendida;
    }

    @Override
    public void encender() {
        this.encendida = true;
    }

    @Override
    public void apagar() {
        this.encendida = false;
    }

    @Override
    public String toString() {
        return "Computadora{" +
                "encendida=" + encendida +
                '}';
    }
}
