package com.patterns.structural.adapter.caso4;

public class LamparaInglesa {

    private boolean isOn;

    public boolean isOn() {
        return isOn;
    }
    public void on () {
        isOn=true;
    }

    public void off() {
        isOn=false;
    }

    @Override
    public String toString() {
        return "LamparaInglesa{" +
                "isOn=" + isOn +
                '}';
    }
}
