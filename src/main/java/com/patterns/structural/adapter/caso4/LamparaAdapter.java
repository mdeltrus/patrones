package com.patterns.structural.adapter.caso4;

public class LamparaAdapter implements ElectroDomestico {

    private LamparaInglesa lampara;

    public LamparaAdapter(LamparaInglesa lampara) {
        this.lampara = lampara;
    }

    @Override
    public boolean isEncendida() {
        return this.lampara.isOn();
    }

    @Override
    public void encender() {
        this.lampara.on();
    }

    @Override
    public void apagar() {
        this.lampara.off();
    }

    @Override
    public String toString() {
        return "LamparaAdapter{" +
                "lampara=" + lampara +
                '}';
    }
}
