package com.patterns.structural.adapter.caso4;

public class Lampara implements ElectroDomestico {

    private boolean encendida;

    public boolean isEncendida() {
        return encendida;
    }

    @Override
    public void encender() {
        encendida=true;
    }

    @Override
    public void apagar() {
        encendida=false;
    }

    @Override
    public String toString() {
        return "Lampara{" +
                "encendida=" + encendida +
                '}';
    }
}