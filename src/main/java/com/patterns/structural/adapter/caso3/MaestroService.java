package com.patterns.structural.adapter.caso3;

public interface MaestroService {

    void save(Maestro maestro);
}
