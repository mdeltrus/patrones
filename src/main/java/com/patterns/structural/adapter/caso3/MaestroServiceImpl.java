package com.patterns.structural.adapter.caso3;

public class MaestroServiceImpl  implements MaestroService{
    @Override
    public void save(Maestro maestro) {
        System.out.println("EVENTO GUARDAR " + maestro);
    }

}
