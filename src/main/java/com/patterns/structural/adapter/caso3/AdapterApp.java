package com.patterns.structural.adapter.caso3;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AdapterApp {

    public static void main(String[] args) {

        RegistroService registro = new RegistroServiceImpl();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fechaNcimiento = LocalDate.parse("23/11/1990", dateTimeFormatter);

        Estudiante estudiante = new Estudiante("JULIO IGLESIAS", fechaNcimiento
                , "0012311990000Z","M");

        System.out.println("ESTUDIANTE: " + estudiante);

        registro.procesar(estudiante);

    }
}
