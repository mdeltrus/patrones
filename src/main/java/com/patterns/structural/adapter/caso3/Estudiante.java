package com.patterns.structural.adapter.caso3;

import java.time.LocalDate;

public class Estudiante {
    String nombreApellidos;
    LocalDate fechaNacimiento;
    String numCedula;
    String sexo;

    public Estudiante(String nombreApellidos, LocalDate fechaNacimiento, String numCedula, String sexo) {
        this.nombreApellidos = nombreApellidos;
        this.fechaNacimiento = fechaNacimiento;
        this.numCedula = numCedula;
        this.sexo = sexo;
    }

    public String getNombreApellidos() {
        return nombreApellidos;
    }

    public void setNombreApellidos(String nombreApellidos) {
        this.nombreApellidos = nombreApellidos;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNumCedula() {
        return numCedula;
    }

    public void setNumCedula(String numCedula) {
        this.numCedula = numCedula;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Override
    public String toString() {
        return "Estudiante{" +
                "nombreApellidos='" + nombreApellidos + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", numCedula='" + numCedula + '\'' +
                ", sexo='" + sexo + '\'' +
                '}';
    }
}
