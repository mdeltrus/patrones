package com.patterns.structural.adapter.caso3;


public class MaestroAdapter {
    public static Maestro getMaestro(Estudiante estudiante) {
        return new Maestro(estudiante.getNombreApellidos(), estudiante.getFechaNacimiento()
                , estudiante.getNumCedula(), estudiante.sexo);
    }
}
