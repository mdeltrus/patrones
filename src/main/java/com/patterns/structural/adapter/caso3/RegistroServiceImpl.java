package com.patterns.structural.adapter.caso3;

public class RegistroServiceImpl implements RegistroService{

    MaestroService servicio = new MaestroServiceImpl();

    @Override
    public void procesar(Estudiante estudiante) {
        servicio.save(MaestroAdapter.getMaestro(estudiante));
    }
}
