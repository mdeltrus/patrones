package com.patterns.structural.adapter.caso3;

public interface RegistroService {

    void procesar(Estudiante estudiante);
}
