package com.patterns.structural.adapter.caso2;

public class AdapterApp {

    public static void main(String[] args) {
        SocketAdapter sockAdapter = new SocketAdapterImpl();
        Volt v3 = sockAdapter.get3Volt();
        Volt v12 = sockAdapter.get12Volt();
        Volt v120 = sockAdapter.get120Volt();

        System.out.println("--------------------------------------------");
        System.out.println("v3 volts: "+v3.getVolts());
        System.out.println("v12 volts: "+v12.getVolts());
        System.out.println("v120 volts: "+v120.getVolts());
    }

}
