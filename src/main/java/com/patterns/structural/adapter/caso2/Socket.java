package com.patterns.structural.adapter.caso2;

public class Socket {

    public Volt getVolt(){
        return new Volt(120);
    }
}
