package com.patterns.structural.decorator.caso3;

public interface Car {
    void assemble();
}