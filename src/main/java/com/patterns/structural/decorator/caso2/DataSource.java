package com.patterns.structural.decorator.caso2;

public interface DataSource {
    void writeData(String data);

    String readData();
}
