package com.patterns.structural.decorator.caso1;

interface Widget {
    void draw();
}