package com.patterns.structural.composite.caso3;

public interface Shape {
    void draw(String fillColor);
}
