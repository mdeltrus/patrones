package com.patterns.structural.composite.caso2;

interface File {
    public String getType();
    public Long getSize();
}
