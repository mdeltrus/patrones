package com.patterns.structural.composite.caso2;

public class AviFile implements File{

    private Long size;

    public AviFile(Long size) {
        this.size = size;
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public Long getSize() {
        return this.size;
    }
}
