package com.patterns.structural.composite.caso2;

public class CompositeApp {

    public static void main(String[] args) {
        TextFile serverLog = new TextFile(100L);
        TextFile notes = new TextFile(200L);

        Directory documents = new Directory(50L);
        JavaFile javafile = new JavaFile(100L);
        HtmlFile htmlfile = new HtmlFile(30L);
        documents.addFile(javafile);
        documents.addFile(htmlfile);

        Directory root= new Directory(100L);
        root.addFile(serverLog);
        root.addFile(notes);
        root.addFile(documents);

        System.out.println("File ServerLog Size: " + serverLog.getSize());
        System.out.println("Directory documents Size: " + documents.getSize());// output : 100
        System.out.println("File root Size: " + root.getSize());    // output : 300
    }
}
