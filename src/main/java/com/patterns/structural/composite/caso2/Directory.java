package com.patterns.structural.composite.caso2;

import java.util.ArrayList;
import java.util.List;

public class Directory implements File {
    private Long size;
    private List<File> files = new ArrayList<>();

    public Directory(Long size) {
        this.size = size;
    }

    public void addFile(File file) {
        files.add(file);
    }

    @Override
    public String getType() {
        return "directory";
    }
    @Override
    public Long getSize() {
        Long size = 0L;
        for (File file : files) {
            size = size + file.getSize();
        }

        return size;
    }
}
