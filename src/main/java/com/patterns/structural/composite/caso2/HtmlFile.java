package com.patterns.structural.composite.caso2;

public class HtmlFile implements File{

    private Long size;

    public HtmlFile(Long size) {
        this.size = size;
    }

    @Override
    public String getType() {
        return "txt";
    }

    @Override
    public Long getSize() {
        return this.size;
    }
}
