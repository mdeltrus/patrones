package com.patterns.structural.composite.caso2;

public class JpegFile implements File{

    private Long size;

    public JpegFile(Long size) {
        this.size = size;
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public Long getSize() {
        return this.size;
    }
}
