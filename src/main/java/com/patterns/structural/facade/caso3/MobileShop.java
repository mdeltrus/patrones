package com.patterns.structural.facade.caso3;

public interface MobileShop {
    void modelNo();
    void price();
}
