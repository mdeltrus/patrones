package com.patterns.utils;

public class Util {

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }
}
