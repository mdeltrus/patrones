package com.patterns.behavioral.observer.caso5;

import java.time.Instant;

public class Mensaje implements Alert {

    private final String content;

    Mensaje(String content) {
        this.content = content + " generated at "+ Instant.now();
    }

    @Override
    public String toString() {
        return "Mensaje{" +
                "content='" + content + '\'' +
                '}';
    }
}
