package com.patterns.behavioral.observer.caso5;


import java.util.concurrent.SubmissionPublisher;

public class ObserverApp {

    public static void main(String[] args) {

        SubmissionPublisher<Alert> publisher = new Publicador();
        SubscriptorA subscriptor = new SubscriptorA();
        NotificadorB notificador = new NotificadorB();

        publisher.subscribe(subscriptor);
        publisher.subscribe(notificador);

        publisher.submit(new Mensaje("prueba observer"));

        publisher.close();
    }
}
