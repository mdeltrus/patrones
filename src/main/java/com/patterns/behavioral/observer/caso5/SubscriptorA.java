package com.patterns.behavioral.observer.caso5;

import java.util.concurrent.Flow;

public class SubscriptorA implements Flow.Subscriber<Alert>{
    Flow.Subscription subscriptor;

    @Override
    public void onSubscribe(Flow.Subscription subscriptor) {
        this.subscriptor = subscriptor;
        this.subscriptor.request(10);
        System.out.println("Registrado: SubscriptorA");

    }

    @Override
    public void onNext(Alert item) {
        System.out.println("Accion disparada desde: SubscriptorA");
        subscriptor.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("onError SubscriptorA");
    }

    @Override
    public void onComplete() {
        System.out.println("Terminando SubscriptorA");
    }

    public Flow.Subscription getSubscriptor() {
        return subscriptor;
    }
}
