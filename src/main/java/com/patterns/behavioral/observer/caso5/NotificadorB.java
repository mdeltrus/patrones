package com.patterns.behavioral.observer.caso5;

import java.util.concurrent.Flow;

public class NotificadorB implements Flow.Subscriber<Alert> {
    Flow.Subscription subscriptor;

    @Override
    public void onSubscribe(Flow.Subscription subscriptor) {
        this.subscriptor = subscriptor;
        this.subscriptor.request(10);
        System.out.println("Registrado: NotificadorB");

    }

    @Override
    public void onNext(Alert item) {
        System.out.println("Accion disparada desde: NotificadorB");
        subscriptor.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("onError NotificadorB");
    }

    @Override
    public void onComplete() {
        System.out.println("Terminando NotificadorB");
    }

    public Flow.Subscription getSubscriptor() {
        return subscriptor;
    }
}
