package com.patterns.behavioral.observer.caso1;

import java.io.File;

public interface EventListener {
    void update(String eventType, File file);
}
