package com.patterns.behavioral.strategy.caso1;

public interface PayStrategy {
    boolean pay(int paymentAmount);
    void collectPaymentDetails();
}