package com.patterns.behavioral.strategy.caso5;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;
import com.patterns.behavioral.strategy.caso5.algoritmos.LiquidacionPorDespidoInjustificado;
import com.patterns.behavioral.strategy.caso5.algoritmos.LiquidacionPorDespidoJustificado;
import com.patterns.behavioral.strategy.caso5.algoritmos.LiquidacionPorRenunciaVoluntaria;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class StrategyApp {

    public static void main(String[] args) {

        System.out.println("***** LIQUIDACON POR DESPIDO INJUSTIFICADOM *****");
        System.out.println("---------------------------------------------------------");
        LocalDate fechaIniContrato = LocalDate.parse("08/05/2021", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDate fechaFinContrato = LocalDate.parse("30/11/2022", DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        Liquidacion liquidacionPorDespidoInjustificado = new LiquidacionPorDespidoInjustificado();
        liquidacionPorDespidoInjustificado.setFechaIniContrato(fechaIniContrato);
        liquidacionPorDespidoInjustificado.setFechaFinContrato(fechaFinContrato);
        liquidacionPorDespidoInjustificado.liquidar();


        System.out.println("\n***** LIQUIDACON POR DESPIDO JUTIFICADO *****");
        System.out.println("---------------------------------------------------------");
        Liquidacion liquidacionPorDespidoJustificado = new LiquidacionPorDespidoJustificado();
        liquidacionPorDespidoJustificado.setFechaIniContrato(fechaIniContrato);
        liquidacionPorDespidoJustificado.setFechaFinContrato(fechaFinContrato);
        liquidacionPorDespidoJustificado.liquidar();

        System.out.println("\n***** LIQUIDACON POR RENUNCIA VOLUNTARIA *****");
        System.out.println("---------------------------------------------------------");
        Liquidacion liquidacionPorRenunciaVoluntaria = new LiquidacionPorRenunciaVoluntaria();
        liquidacionPorRenunciaVoluntaria.setFechaIniContrato(fechaIniContrato);
        liquidacionPorRenunciaVoluntaria.setFechaFinContrato(fechaFinContrato);
        liquidacionPorRenunciaVoluntaria.liquidar();

    }
}
