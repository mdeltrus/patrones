package com.patterns.behavioral.strategy.caso5.algoritmos;

import com.patterns.behavioral.strategy.caso5.componentes.Componente;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class Liquidacion {

    private List<Componente> algoritmos = new ArrayList<>();


    private LocalDate fechaUltimoPago;
    private BigDecimal salarioMensual;
    private LocalDate fechaIniContrato;
    private LocalDate fechaFinContrato;
    private double horasExtrasAcumuladas;
    private double diasVacacionesConGozo;
    private double diasVacacaionesSinGozo;

    private BigDecimal montoSalariosPendiente;
    private BigDecimal montoHorasExtras;
    private BigDecimal montoVacacionesSinGozo;
    private BigDecimal montoAguinaldoProporcional;
    private BigDecimal montoIndemnizacion;

    private Boolean calculoAnhosAnteriores20Dias;

    public abstract void init();

    public abstract void liquidar();

    protected void calcular(){
        for(Componente algoritmo : this.algoritmos){
            algoritmo.procesar(this);
        }
    }

    public void agregarComponente(Componente algoritmo){
        this.algoritmos.add(algoritmo);
    }

    public LocalDate getFechaUltimoPago() {
        return fechaUltimoPago;
    }

    public void setFechaUltimoPago(LocalDate fechaUltimoPago) {
        this.fechaUltimoPago = fechaUltimoPago;
    }

    public BigDecimal getSalarioMensual() {
        return salarioMensual;
    }

    public void setSalarioMensual(BigDecimal salarioMensual) {
        this.salarioMensual = salarioMensual;
    }

    public LocalDate getFechaIniContrato() {
        return fechaIniContrato;
    }

    public void setFechaIniContrato(LocalDate fechaIniContrato) {
        this.fechaIniContrato = fechaIniContrato;
    }

    public LocalDate getFechaFinContrato() {
        return fechaFinContrato;
    }

    public void setFechaFinContrato(LocalDate fechaFinContrato) {
        this.fechaFinContrato = fechaFinContrato;
    }

    public double getHorasExtrasAcumuladas() {
        return horasExtrasAcumuladas;
    }

    public void setHorasExtrasAcumuladas(double horasExtrasAcumuladas) {
        this.horasExtrasAcumuladas = horasExtrasAcumuladas;
    }

    public double getDiasVacacionesConGozo() {
        return diasVacacionesConGozo;
    }

    public void setDiasVacacionesConGozo(double diasVacacionesConGozo) {
        this.diasVacacionesConGozo = diasVacacionesConGozo;
    }

    public double getDiasVacacaionesSinGozo() {
        return diasVacacaionesSinGozo;
    }

    public void setDiasVacacaionesSinGozo(double diasVacacaionesSinGozo) {
        this.diasVacacaionesSinGozo = diasVacacaionesSinGozo;
    }

    public BigDecimal getMontoSalariosPendiente() {
        return montoSalariosPendiente;
    }

    public void setMontoSalariosPendiente(BigDecimal montoSalariosPendiente) {
        this.montoSalariosPendiente = montoSalariosPendiente;
    }

    public BigDecimal getMontoHorasExtras() {
        return montoHorasExtras;
    }

    public void setMontoHorasExtras(BigDecimal montoHorasExtras) {
        this.montoHorasExtras = montoHorasExtras;
    }

    public BigDecimal getMontoVacacionesSinGozo() {
        return montoVacacionesSinGozo;
    }

    public void setMontoVacacionesSinGozo(BigDecimal montoVacacionesSinGozo) {
        this.montoVacacionesSinGozo = montoVacacionesSinGozo;
    }

    public BigDecimal getMontoAguinaldoProporcional() {
        return montoAguinaldoProporcional;
    }

    public void setMontoAguinaldoProporcional(BigDecimal montoAguinaldoProporcional) {
        this.montoAguinaldoProporcional = montoAguinaldoProporcional;
    }

    public BigDecimal getMontoIndemnizacion() {
        return montoIndemnizacion;
    }

    public void setMontoIndemnizacion(BigDecimal montoIndemnizacion) {
        this.montoIndemnizacion = montoIndemnizacion;
    }

    public Boolean getCalculoAnhosAnteriores20Dias() {
        return calculoAnhosAnteriores20Dias;
    }

    public void setCalculoAnhosAnteriores20Dias(Boolean calculoAnhosAnteriores20Dias) {
        this.calculoAnhosAnteriores20Dias = calculoAnhosAnteriores20Dias;
    }
}
