package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

public class VacacionesSinGozoComponent implements Componente {

    @Override
    public void procesar(Liquidacion entity) {

        LocalDate fechaIni = entity.getFechaIniContrato();
        LocalDate fechaFin = entity.getFechaFinContrato();
        BigDecimal salario = entity.getSalarioMensual();
        double diasVacacionesConGozo = entity.getDiasVacacionesConGozo();

        Period diferencia = fechaIni.until(fechaFin);
        int years = diferencia.getYears();
        int months = diferencia.getMonths();
        int days = diferencia.getDays();

        double vacaciones = years*30 + months*2.5 + (days/30)*2.5;
        BigDecimal vacacionesSinGozo = BigDecimal.valueOf(vacaciones -  diasVacacionesConGozo);

        BigDecimal montoVacaciones = (salario.divide(BigDecimal.valueOf(30))
                .multiply(vacacionesSinGozo));

        entity.setDiasVacacaionesSinGozo(vacacionesSinGozo.doubleValue());
        entity.setMontoVacacionesSinGozo(montoVacaciones);
        System.out.println(String.format("Vacaciones:\t\t\t\t%.2f" ,entity.getMontoVacacionesSinGozo()));

    }
}
