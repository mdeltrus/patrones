package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

public class HorasExtrasAcumuladasComponent implements Componente{
    @Override
    public void procesar(Liquidacion entity) {
        entity.setHorasExtrasAcumuladas(11.4);
    }
}
