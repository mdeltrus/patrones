package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DatosContrato implements Componente{
    @Override
    public void procesar(Liquidacion entity) {
        entity.setFechaIniContrato(LocalDate.parse("15/11/2021", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }
}
