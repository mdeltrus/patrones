package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FechaUltimoPagoSalarioComponent implements Componente{
    @Override
    public void procesar(Liquidacion entity) {
        entity.setFechaUltimoPago(LocalDate.parse("30/10/2022", DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }
}
