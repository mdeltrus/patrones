package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.math.BigDecimal;

public class HorasExtrasComponent implements Componente {

    @Override
    public void procesar(Liquidacion entity) {

        BigDecimal salarioMensual = entity.getSalarioMensual();
        BigDecimal salarioPorHora = salarioMensual.divide(BigDecimal.valueOf(30)).divide(BigDecimal.valueOf(8));

        double horasExtras = entity.getHorasExtrasAcumuladas();
        BigDecimal montoHorasExtras = salarioPorHora.multiply(BigDecimal.valueOf(horasExtras));
        entity.setMontoHorasExtras(montoHorasExtras);
        System.out.println(String.format("Horas Extras:\t\t\t%.2f" ,entity.getMontoHorasExtras()));

    }
}
