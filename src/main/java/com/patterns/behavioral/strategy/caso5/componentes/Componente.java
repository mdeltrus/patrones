package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

public interface Componente {

    void procesar(Liquidacion entity);
}
