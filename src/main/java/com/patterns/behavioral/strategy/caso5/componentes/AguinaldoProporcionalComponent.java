package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;
import com.patterns.structural.decorator.caso2.DataSource;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class AguinaldoProporcionalComponent implements Componente {

    @Override
    public void procesar(Liquidacion entity) {

        BigDecimal salario = entity.getSalarioMensual();

        LocalDate fechaFinContrato = entity.getFechaFinContrato();

        LocalDate fechaIniCorte = fechaFinContrato
                .withDayOfYear(1).minusMonths(1);

        LocalDate fechaFinCorte = fechaFinContrato
                .withMonth(12).withDayOfMonth(1).minusDays(1);

        long diasPeriodoAguinaldo = ChronoUnit.DAYS.between(fechaIniCorte, fechaFinCorte)+1;
        long diasCalculoAguinaldo = ChronoUnit.DAYS.between(fechaIniCorte, fechaFinContrato)+1;

        BigDecimal salarioDiario = salario.divide(BigDecimal.valueOf(diasPeriodoAguinaldo), 2, RoundingMode.CEILING);
        BigDecimal aguinaldo = salarioDiario.multiply(BigDecimal.valueOf(diasCalculoAguinaldo));

        entity.setMontoAguinaldoProporcional(aguinaldo);
        System.out.println(String.format("Aguinaldo proporcional:\t%.2f" ,entity.getMontoAguinaldoProporcional()));

    }
}
