package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.math.BigDecimal;

public class SalarioComponent implements Componente{
    @Override
    public void procesar(Liquidacion entity) {
        entity.setSalarioMensual(BigDecimal.valueOf(9000));
    }
}
