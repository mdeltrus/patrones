package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.math.BigDecimal;

public class MontoLiquidacionComponent implements Componente{

        @Override
        public void procesar(Liquidacion entity) {

            BigDecimal total = BigDecimal.ZERO;
            total = total.add(entity.getMontoSalariosPendiente() != null ? entity.getMontoSalariosPendiente() : BigDecimal.ZERO);
            total = total.add(entity.getMontoHorasExtras() != null ? entity.getMontoHorasExtras() : BigDecimal.ZERO);
            total = total.add(entity.getMontoAguinaldoProporcional() != null ? entity.getMontoAguinaldoProporcional() : BigDecimal.ZERO);
            total = total.add(entity.getMontoVacacionesSinGozo() != null ? entity.getMontoVacacionesSinGozo() : BigDecimal.ZERO);
            total = total.add(entity.getMontoIndemnizacion() != null ? entity.getMontoIndemnizacion() : BigDecimal.ZERO);

            System.out.println("================================");
            System.out.println(String.format("Total\t\t\t\t\t%.2f" ,total));
        }
}
