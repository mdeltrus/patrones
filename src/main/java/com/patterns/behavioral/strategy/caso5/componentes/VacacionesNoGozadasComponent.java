package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.math.BigDecimal;

public class VacacionesNoGozadasComponent implements Componente {

    @Override
    public void procesar(Liquidacion entity) {
        entity.setVacacionesNoGozadas(BigDecimal.valueOf(2399));
        System.out.println(String.format("%.2f\t\tImpuesto sobre la Renta" ,entity.getVacacionesNoGozadas()));

    }
}
