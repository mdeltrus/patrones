package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.Collections;

public class Indemnizacion2 implements Componente {



    @Override
    public void procesar(Liquidacion entity) {
        /* variables de entrada */
        BigDecimal salario = entity.getSalarioMensual();
        LocalDate fechaIni = entity.getFechaIniContrato();
        LocalDate fechaFin = entity.getFechaFinContrato();

        BigDecimal salarioDiario = salario.divide(BigDecimal.valueOf(30), 4, RoundingMode.HALF_UP);
        BigDecimal salarioMensual20 = salarioDiario.multiply(BigDecimal.valueOf(20));

        LocalDate fechaPivote = fechaFin.minusYears(6);
        fechaPivote = fechaPivote.isBefore(fechaIni) ? fechaIni : fechaPivote;

        Period diferencia = fechaPivote.until(fechaFin);
        int years = diferencia.getYears();

        int last3Years = Collections.min(Arrays.asList(years, 3));
        int lastPreviousYears = years > 3 ? years - 3 : 0;
        lastPreviousYears = lastPreviousYears > 3 ? 3 : lastPreviousYears;

        BigDecimal salarioLast3years = salario.multiply(BigDecimal.valueOf(last3Years));
        BigDecimal salarioLastPreviousYears = BigDecimal.ZERO;

        if(entity.getCalculoAnhosAnteriores20Dias()) {
            salarioLastPreviousYears = salarioMensual20.multiply(BigDecimal.valueOf(lastPreviousYears)).setScale(4, RoundingMode.HALF_EVEN);
        }else {
            salarioLastPreviousYears = salario.multiply(BigDecimal.valueOf(lastPreviousYears)).setScale(4, RoundingMode.HALF_EVEN);
        }

        BigDecimal indemnizacion =
                salarioLast3years.add(salarioLastPreviousYears).setScale(2, RoundingMode.HALF_EVEN);

        entity.setMontoIndemnizacion(indemnizacion);
//        System.out.println(String.format("lastPreviousYears : %d", lastPreviousYears));
//        System.out.println(String.format("salarioLast3years : %.2f", salarioLast3years));
//        System.out.println(String.format("salarioLastPreviousYears : %.2f", salarioLastPreviousYears));
        System.out.println(String.format("Indemnizacion:\t\t\t%.2f" ,entity.getMontoIndemnizacion()));
    }
}
