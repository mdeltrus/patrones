package com.patterns.behavioral.strategy.caso5.algoritmos;

import com.patterns.behavioral.strategy.caso5.componentes.*;

public class LiquidacionPorRenunciaVoluntaria extends Liquidacion{

        @Override
        public void init(){
            /* algoritmos de consulta a origen de datos*/
            this.agregarComponente(new DatosContrato());
            this.agregarComponente(new SalarioComponent());
            this.agregarComponente(new FechaUltimoPagoSalarioComponent());
            this.agregarComponente(new VacacionesConGozoComponent());
            this.agregarComponente(new HorasExtrasAcumuladasComponent());

            /* algoritmos de calculos*/
            this.agregarComponente(new SalariosPendientesComponent());
            this.agregarComponente(new AguinaldoProporcionalComponent());
            this.agregarComponente(new Indemnizacion());
            this.agregarComponente(new MontoLiquidacionComponent());
        }

        @Override
        public void liquidar() {
            this.init();
            this.calcular();
        }

}
