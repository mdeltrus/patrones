package com.patterns.behavioral.strategy.caso5.algoritmos;

import com.patterns.behavioral.strategy.caso5.componentes.*;

public class LiquidacionPorDespidoJustificado extends Liquidacion{

    @Override
    public void init(){
        /* algoritmos de consulta a origen de datos*/
        this.agregarComponente(new DatosContrato());
        this.agregarComponente(new SalarioComponent());
        this.agregarComponente(new FechaUltimoPagoSalarioComponent());
        this.agregarComponente(new VacacionesConGozoComponent());
        this.agregarComponente(new HorasExtrasAcumuladasComponent());

        /* algoritmos de calculos*/
        this.agregarComponente(new SalariosPendientesComponent());
        this.agregarComponente(new HorasExtrasComponent());
        this.agregarComponente(new VacacionesSinGozoComponent());
        this.agregarComponente(new AguinaldoProporcionalComponent());
        this.agregarComponente(new MontoLiquidacionComponent());
    }

    @Override
    public void liquidar() {
        this.init();
        this.calcular();
    }

}
