package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class AguinaldoProporcional2Component implements Componente {

    @Override
    public void procesar(Liquidacion entity) {

        BigDecimal salario = entity.getSalarioMensual();
        LocalDate fechaIniContrato = entity.getFechaIniContrato();
        LocalDate fechaFinContrato = entity.getFechaFinContrato();


        LocalDate fechaIniCorte = fechaFinContrato
                .withDayOfYear(1).minusMonths(1);

        LocalDate fechaFinCorte = fechaFinContrato
                .withMonth(12).withDayOfMonth(1).minusDays(1);

        long diasPeriodoAguinaldo = ChronoUnit.DAYS.between(fechaIniCorte, fechaFinCorte)+1;
        BigDecimal salarioDiario = salario.divide(BigDecimal.valueOf(diasPeriodoAguinaldo), 2, RoundingMode.CEILING);
        //BigDecimal aguinaldo = salarioDiario.multiply(BigDecimal.valueOf(diasPeriodoAguinaldo));
        BigDecimal aguinaldo = salario;

        long anhosAntiguedad = ChronoUnit.YEARS.between(fechaIniContrato,fechaFinContrato);
        if( anhosAntiguedad < 5){
            long diasCalculoAguinaldo = ChronoUnit.DAYS.between(fechaIniCorte, fechaFinContrato)+1;
            aguinaldo = salarioDiario.multiply(BigDecimal.valueOf(diasCalculoAguinaldo));
        }

        entity.setMontoAguinaldoProporcional(aguinaldo);
        System.out.println(String.format("Aguinaldo proporcional:\t%.2f" ,entity.getMontoAguinaldoProporcional()));

    }
}
