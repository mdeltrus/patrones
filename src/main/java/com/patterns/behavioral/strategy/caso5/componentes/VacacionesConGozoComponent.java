package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

public class VacacionesConGozoComponent implements Componente{

    @Override
    public void procesar(Liquidacion entity) {
        entity.setDiasVacacionesConGozo(15.4);
    }
}
