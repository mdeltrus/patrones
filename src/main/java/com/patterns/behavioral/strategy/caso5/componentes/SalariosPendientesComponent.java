package com.patterns.behavioral.strategy.caso5.componentes;


import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;

public class SalariosPendientesComponent implements Componente {

    @Override
    public void procesar(Liquidacion entity) {

        BigDecimal salario = entity.getSalarioMensual();
        LocalDate fechaUltimoPago = entity.getFechaUltimoPago();
        LocalDate fechaFinContrato = entity.getFechaFinContrato();

        Period periodo = fechaUltimoPago.until(fechaFinContrato);
        int years = periodo.getYears();
        int months = periodo.getMonths();
        int days = periodo.getDays();

        int meses = (years * 12) + months;
        BigDecimal salarioEnDias = BigDecimal.valueOf(days)
                .divide(BigDecimal.valueOf(30), 2, RoundingMode.CEILING)
                .multiply(salario);

        BigDecimal salarioSincobrar =
                salario.multiply(BigDecimal.valueOf(meses))
                        .add(salarioEnDias);

        entity.setMontoSalariosPendiente(salarioSincobrar);
        System.out.println(String.format("Salario Pendiente:\t\t%.2f" ,entity.getMontoSalariosPendiente()));

    }
}
