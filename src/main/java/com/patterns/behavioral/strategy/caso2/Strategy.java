package com.patterns.behavioral.strategy.caso2;

public interface Strategy {
    void solve();
}
