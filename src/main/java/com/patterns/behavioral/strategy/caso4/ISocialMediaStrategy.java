package com.patterns.behavioral.strategy.caso4;

public interface ISocialMediaStrategy {

    void connectTo(String friendName);
}
