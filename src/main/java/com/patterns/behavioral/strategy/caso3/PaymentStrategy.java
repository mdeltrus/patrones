package com.patterns.behavioral.strategy.caso3;

public interface PaymentStrategy {

    public void pay(int amount);
}
