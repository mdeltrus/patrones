package com.patterns.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UtilText {

    @ParameterizedTest
    @ValueSource(ints = {0, 2, 4, 6})
    void given_4_even_numbers_should_return_true(int number) {
        assertTrue(Util.isEven(number));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/prueba.csv", numLinesToSkip = 1)
    void given_5_even_numbers_should_return_true(int number) {
        assertTrue(Util.isEven(number));
    }

}
