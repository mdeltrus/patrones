package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;
import com.patterns.behavioral.strategy.caso5.algoritmos.LiquidacionPorDespidoInjustificado;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class IndemnizacionComponentTest {

    @ParameterizedTest
    @CsvFileSource(numLinesToSkip = 1, resources = "/indemnizacion.csv")
    public void calculo_indemnizacion(int in, BigDecimal salario, String fechaIni, String fechaFin, BigDecimal indemnizacion){

        LocalDate fechaInicioContrato = LocalDate.parse(fechaIni, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDate fechaFInalizaContrato = LocalDate.parse(fechaFin, DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        Liquidacion liquidacion = new LiquidacionPorDespidoInjustificado();
        liquidacion.setFechaIniContrato(fechaInicioContrato);
        liquidacion.setFechaFinContrato(fechaFInalizaContrato);
        liquidacion.setSalarioMensual(salario);

        Componente algoritmo = new Indemnizacion();
        algoritmo.procesar(liquidacion);

        Assertions.assertEquals(indemnizacion, liquidacion.getMontoIndemnizacion().setScale(2, RoundingMode.HALF_EVEN));

    }

    @ParameterizedTest
    @CsvFileSource(numLinesToSkip = 1, resources = "/indemnizacion_variante2.csv")
    public void calculo_indemnizacion_variante2(int in, BigDecimal salario, String fechaIni, String fechaFin, BigDecimal indemnizacion, int anteriores) {

        LocalDate fechaInicioContrato = LocalDate.parse(fechaIni, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDate fechaFInalizaContrato = LocalDate.parse(fechaFin, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        Liquidacion liquidacion = new LiquidacionPorDespidoInjustificado();
        liquidacion.setFechaIniContrato(fechaInicioContrato);
        liquidacion.setFechaFinContrato(fechaFInalizaContrato);
        liquidacion.setSalarioMensual(salario);
        liquidacion.setCalculoAnhosAnteriores20Dias(anteriores == 1);

        Componente algoritmo = new Indemnizacion2();
        algoritmo.procesar(liquidacion);

        Assertions.assertEquals(indemnizacion, liquidacion.getMontoIndemnizacion().setScale(2, RoundingMode.HALF_EVEN));

    }
}
