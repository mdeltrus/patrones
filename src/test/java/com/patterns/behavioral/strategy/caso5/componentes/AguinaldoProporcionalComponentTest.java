package com.patterns.behavioral.strategy.caso5.componentes;

import com.patterns.behavioral.strategy.caso5.algoritmos.Liquidacion;
import com.patterns.behavioral.strategy.caso5.algoritmos.LiquidacionPorDespidoInjustificado;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AguinaldoProporcionalComponentTest {


    @ParameterizedTest
    @CsvFileSource(resources = "/aguinaldoProporcional.csv", numLinesToSkip = 1)
    public void calculo_aguinaldo_proporcional(int id, BigDecimal salario, String finContrato, String iniAguinaldo, String finAguinaldo, BigDecimal salarioDia, BigDecimal aguinaldo){

        Liquidacion liquidacion = new LiquidacionPorDespidoInjustificado();
        liquidacion.setSalarioMensual(salario);
        liquidacion.setFechaFinContrato(LocalDate.parse(finContrato, DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        Componente algoritmo = new AguinaldoProporcionalComponent();
        algoritmo.procesar(liquidacion);

        Assertions.assertEquals(aguinaldo, liquidacion.getMontoAguinaldoProporcional());


    }

    @ParameterizedTest
    @CsvFileSource(resources = "/aguinaldoProporcional2.csv", numLinesToSkip = 1)
    public void calculo_aguinaldo_proporcional_2(int id, BigDecimal salario, String finContrato, String iniAguinaldo, String finAguinaldo, BigDecimal salarioDia, BigDecimal aguinaldo){

        Liquidacion liquidacion = new LiquidacionPorDespidoInjustificado();
        liquidacion.setSalarioMensual(salario);
        liquidacion.setFechaFinContrato(LocalDate.parse(finContrato, DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        Componente algoritmo = new AguinaldoProporcionalComponent();
        algoritmo.procesar(liquidacion);

        Assertions.assertEquals(aguinaldo, liquidacion.getMontoAguinaldoProporcional());


    }

    @ParameterizedTest
    @CsvFileSource(resources = "/aguinaldoProporcional3.csv", numLinesToSkip = 1)
    public void calculo_aguinaldo_proporcional_3(int id, BigDecimal salario, String iniContrato, String finContrato, String iniAguinaldo, String finAguinaldo, int antiguedad, BigDecimal salarioDia, BigDecimal aguinaldo){

        Liquidacion liquidacion = new LiquidacionPorDespidoInjustificado();
        liquidacion.setSalarioMensual(salario);
        liquidacion.setFechaIniContrato(LocalDate.parse(iniContrato, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        liquidacion.setFechaFinContrato(LocalDate.parse(finContrato, DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        Componente algoritmo = new AguinaldoProporcional2Component();
        algoritmo.procesar(liquidacion);

        Assertions.assertEquals(aguinaldo, liquidacion.getMontoAguinaldoProporcional());


    }
}
