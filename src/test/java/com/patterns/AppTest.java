package com.patterns;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class AppTest{

    @Test
    public void fechas(){

        LocalDate ini = LocalDate.parse("15/10/2021", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDate fin = LocalDate.parse("30/11/2022", DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        //int years = Period.between(ini, fin).getYears();
        Period diferencia = ini.until(fin);
        int years = diferencia.getYears();
        int months = diferencia.getMonths();
        int days = diferencia.getDays();

        System.out.println("anho: " + years + ", months: " + months + ", days: " + days);


    }

}
