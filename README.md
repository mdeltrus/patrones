# Material de practica para el curso de Patrones de Diseño - UCEM

- [ ] [Enlace descarga presntacion](https://gitlab.com/mdeltrus/patrones/-/blob/main/patrones.odp)

# A LAS 12:00 DEL MEDIO DIA del 27/11/2022, SE CIERRA EL REPOSITORIO PARA QUE PUEDAN HACER PUSH

## 1er Evaluación

Las soluciones a cada uno de los ejercicios es individual, se evaluara el analisis aplicado y aplicacion del avancen en el contenido hasta el dia viernes 25 de Noviembre 2022. Cada ejercicio sera asignado a un maximo de 5 estudiantes. 

La solucion al problema por estudiante debera ser unica. Podran existir elementos de logicas similares pero a la practica la implementacion dependera de la creatividad de cada uno. Ningun proyecto puede ser igual.

Evaluacion minima de aprobacion 80% 

Cada estudiante debera trabajar en una rama individual la cual subiran al repositorio remoto, nombraran la rama con su nombre completo utilizando la anotacion snake_case 
Ejemplo

Ejemplo nombre de rama: ejercicio1_rafael_antonio_garcia_estrada

# Ejercicio No 1:

## Estudiantes

1. Arioc Abraham Moreno Delgado
2. Daniel Alexander Prado Amoretty
3. Agustin Enrique Rocha Monjes (AUN NO PRESENTA PRUEBA)
4. Josue David Reyes Molina
5. Dayton Enmanuel Martinez Ruiz (AUN NO PRESENTA PRUEBA)

## Plantamiento del problema

Se requiere un modelo de sistema en linea de alquiler de vehículos, el cual permita seleccionar al usuario los distintos paquetes y promociones disponibles configurados según necesidades del usuario. El usuario estará identificado por un código único de cliente que deberá ingresar para identificarse en el sistema. Se deben identificar ciertos aspectos del cliente que deberán ser completadas un cuestionario que al finalizar, el sistema mostrara visualmente las sugerencias de paquetes ajustados a su necesidad y/o mostrar promociones disponibles según temporada. 

Cuestionario:

1. ¿Fecha previsto de arriendo considerando el inicio y fin?
2. ¿Motivo del arriendo? Trabajo, Vacaciones, Otro
3. ¿Estado civil?
4. ¿Cantidad de miembros de la familia?
5. ¿Posee hijos?
6. ¿Posee mascota?

Los clientes estan clasificados segun historial de eventos de alquiler realizados. 

1. Clientes Plata, al menos 15 eventos. Exoneracion por dias excedidos 1.5%
2. Clientes Gold, al menos 20 eventos. Exnoneracion por dias excedidos 2.2% 
3. Clientes Platimun, al menos 30 eventos. Exoneracon por dias excedido 3%

Asi mismo a los clientes Platinum tendran un descuento permanente de alquiler del 2%, mientras que los clientes Gold el descuento sera del 1% cuando el periodo de alquiler pactado exceda los 30 dias. Las exoneraciones y descuentosa formaran parte del precio de los paquetes que ofreceran al cliente.

La temporada debera ser identificada segun el rango de fecha de arriendo que el usuario indique:

1. Primavera del 21/marzo al 22/junio 
2. Verano del 22/junio al 24/septiembre
3. Otoño del 24/septiembre al 22/diciembre
4. Invierno 22/diciembre al 21/marzo


Las promociones que aplican segun temporada

1. Primavera: No aplica  
2. Verano: Termo para hielo, kit de protección solar, Sombría, Gafas para el sol
3. Otoño: No aplica
4. Invierno: Plan de seguro asistencial en caso de accidentes

Cada paquete debera estar acompañado por las promociones y exoneraciones aplicadas segun la clasificacon del cliente y encuesta realizada.

Para dar solucion al modelo propuesto, se deben aplicar los principios SOLID del desarrollo de software aplicando patrones de diseño ajutados a los problemas identificados en el modelo a representar


# Ejercicio No 2:

## Estudiantes

1. Mauriell Clemente Garcia Perez (AUN NO PRESENTA PRUEBA)
2. Juan Francisco Aburto Martinez
3. Julissa del Carmen Lopez Rivas
4. Manuel de Jesus Caceres Mercado
5. Josue Adan Sanchez Gomez


## Plantamiento del problema

Las iglesias románicas suelen variar en función de su importancia. Las iglesias rurales o menores constan habitualmente de una sencilla nave y un ábside sin crucero saliente. Las pocas ventanas de las que constan suelen ser más altas que anchas, de arco doble y sin decorar exteriormente, cerradas con simples telas blancas enceradas o impregnada con trementina. 

Las iglesias de mayor importancia, como en monasterios o santuarios de peregrinación, se componen de una planta basilical latina con tres o cinco naves y crucero de brazos salientes. En el testero o cabecera, que siempre mira a oriente, se hallan tres o cinco ábsides semicirculares de frente o formando corona, llevando cada uno de ellos tres ventanas en su muro, cerradas con vidrieras incoloras o en color en las más suntuosas 

Se pretende desarrollar un modelo virtual que permita la construccion de iglesias romanicas, lo cual permitira analizar las distintas variantes existentes y los nuevos modelos a produccir segun las convinaciones de los distintos elementos que las conforman

Para dar solucion al modelo propuesto, se deben aplicar los principios SOLID del desarrollo de software aplicando patrones de diseño ajutados a los problemas identificados en el modelo a representar


# Ejercicio No 3:

## Estudiantes

1. Donald Jose Munguia Guadamuz
2. Karen Lisseth Alegria Reyes
3. Daniela Carolina Silva Laguna
4. Jose Abraham Vargas Perez
5. Juan Ramon Moreno Lopez

## Plantamiento del problema

Se quiere implementar un sistema de recolección de datos meteorológicos con el fin de generar mapas climáticos en forma periódica, utilizando datos recopilados por estaciones 
meteorológicas remotas desatendidas y otras fuentes de datos tales como observadores humanos, globos y satélites. 

Las estaciones climáticas transmiten sus datos a un ordenador central en respuesta a una petición de éste. El ordenador central valida los datos recolectados y los integra con los datos de otras fuentes. Estos datos integrados son archivados y, usando los datos archivados y una base de datos de mapas digitalizados se confecciona un conjunto de mapas climáticos locales, que pueden ser impresos o mostrados por pantalla. 

La estación climática es un paquete de instrumentos meteorológicos controlados por software, que recopila datos, ejecuta procesamiento sobre los mismos y transmite los datos al ordenador central para su posterior procesamiento. 

Los instrumentos meteorológicos incluyen: termómetros de suelo y aire, anemómetros, veletas, barómetros y pluviómetros. 

Los datos son recolectados cada cinco minutos. Desde el ordenador central se puede activar o desactivar una estación. Mientras está activa la estación y a la espera de recibir una orden del ordenador 
central se realizan calibrados periódicos de los instrumentos y se recolecta la información de los instrumentos. Cuando se recibe una orden desde el ordenador central se deben transmitir los datos recopilados hasta el momento. En este caso, la estación realiza un procesamiento para recopilar el resumen de los datos de cada uno de los instrumentos. Este proceso incluye, realizar un testeo de todos los instrumentos, tras el testeo se debe recopilar la información y por último hacer el resumen de los datos. Los datos recopilados en el resumen son transmitidos al ordenador central.

Considerar la compatibilidad de equipos de fabricacion europeo distintos al estandar utilizado del modelo del ordenador central al momento de sincronizar y recopilar datos de estas estaciones. Tambien se deben considerar los datos obtenidos por humanos que pudieran proporcionar datos en cuyo modelo difieran de la base datos del ordenador central

Para dar solucion al modelo propuesto, se deben aplicar los principios SOLID del desarrollo de software aplicando patrones de diseño ajutados a los problemas identificados en el modelo a representar


# Ejercicio No 4:

## Estudiantes

1. Herlin Daniel Silva Laguna
2. Gustavo Adolfo Garcia Alvarado  (AUN NO PRESENTA PRUEBA)
3. Santos Alberto Ortiz Chavez
4. Moises Antonio Romero Pereira
5. Alejandro Antonio Ruiz Munguia

## Plantamiento del problema

Se requiere desarrollar biblioteca virtual por subscripcion en la que se podrán visualizar libros dependiendo del nivel de acceso y seguridad aplicado al momento de consultar. Siempre se mostrara un demo del libro que consistira en la portada del libro y las primeras 5 paginas, el resto de paginas se disfuminara cada linea para evitar lectura del contenido, lo anterior aplica para usuarios sin inicio de sesion.

Usuarios con inicio de sesion, los libros comprados podran ser visualizados al 100% y habilitada la opcion para descarga en formato PDF, ePub y MOBI. Si el usuario desea comprar un libro, se visualizara un demo con la portada del libro y las primeras 20 paginas habilitadas, permitiendo descargar un demo en PDF del mismo.

Dependiendo de la preferencias de lectura del usuario basados en historial de compras de libros, se realizara una sugerencia para compra de distintos libros relacionados. Para ello los libros deberan estar categorizado segun tematica 

Segun historial de compras y al menos comprado 10 libros, el sistema regalara sin importar la tematica  un libro para premiar la fidelidad del cliente como subscriptor activo

Se deben considerar los libros como objetos unicos que no pueden ser alterados por ninguna circustancia, al momento de mostrar los demos de los libros al usuario 

Para dar solucion al modelo propuesto, se deben aplicar los principios SOLID del desarrollo de software aplicando patrones de diseño ajutados a los problemas identificados en el modelo a representar
